• Aggiunta di un avviso nel caso in cui ci si connetta a reti più vecchie e insicure.
• Aggiunta di una scheda relativa ai protocolli.
• Aggiornamento della traduzione in lingua Italiana fornito da Francesco Buratti.