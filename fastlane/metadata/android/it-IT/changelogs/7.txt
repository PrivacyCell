• Rimozione delle autorizzazioni non necessarie aggiunte da androidx.work.
• Sistemazione del servizio di notifica che non riusciva a riavviarsi da solo in background su Android 12.
• Aggiornamento della traduzione in lingua Italiana fornito da Francesco Buratti.
• Prima traduzione completa in lingua Russa.