• Optionales einfarbiges Icon hinzugefügt (kann in Android >= 13 genutzt werden).
• Privacy Cell kann in System-Backups integriert werden.
• Ziel-API auf 34 (Android 14) angehoben.
• Für neuere Android-Versionen benötigte Berechtigungen dokumentiert.
• Benachrichtigungen der Echtzeit-Überwachung werden alle 15 Minuten automatisch wiederholt.
• Erste komplette deutsche Übersetzung von Bernhard G. Keller.