• Ziel-API auf 33 (Android 13) angehoben.
• Wischgeste zum zum Ablehnen der Benachrichtigungen des Echtzeit-Monitorings unter Android 13 deaktiviert.
• App-Sprachauswahl zur Auswahl der Sprache von Privacy Cell unter Android 13 hinzugefügt.
• Scroll-Position im Einstellungs-Dialog wird nun bei Neustarts beibehalten.
• Erste komplette deutsche Übersetzung von Bernhard G. Keller.