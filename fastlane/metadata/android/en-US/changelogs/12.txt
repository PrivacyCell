• Create an optional monochrome icon (can be used in Android >= 13).
• Allow Privacy Cell to be included in system backups.
• Bump the target API to 34 (Android 14).
• Document permissions needed for newer versions of Android.
• Repopulate the realtime monitoring notification every 15 minutes.