<?xml version="1.0" encoding="utf-8"?>

<!--
  Copyright 2021-2022 Soren Stoutner <soren@stoutner.com>.

  Translation 2021-2022 Francesco Buratti.  Copyright assigned to Soren Stoutner <soren@stoutner.com>.

  This file is part of Privacy Cell <https://www.stoutner.com/privacy-cell>.

  Privacy Cell is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  Privacy Cell is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with Privacy Cell.  If not, see <http://www.gnu.org/licenses/>. -->

<resources>
    <!-- General. -->
    <string name="privacy_cell">Privacy Cell</string>
    <string name="cell">Cella</string>
    <string name="asset_directory">it</string>  <!-- The asset directory string should be translated to match the language code for the directory that contains the HTML files. -->
    <string name="open_navigation_drawer">Apri la scheda di navigazione</string>
    <string name="close_navigation_drawer">Chiudi la scheda di navigazione</string>
    <string name="not_connected">Sembra che il tuo dispositivo non sia connesso ad alcuna cella.</string>

    <!-- Text views.  The `\n\n` code inserts a line break and should be preserved in translations.
        Android removes double spaces, but extra spaces can be manually specified with the Unicode `\u0020` formatting.
        The `%1$s` code inserts variables into the displayed text and should be preserved in translation. -->
    <string name="secure_protocols">Il tuo dispositivo è connesso ad una rete che utilizza protocolli sicuri.\n\nE\' sicura nei confronti di attacchi di tipo stingray IMSI man-in-the-middle.</string>
    <string name="insecure_protocols">Il tuo dispositivo è connesso ad una rete che utilizza protocolli non sicuri.\n\nNon è sicura nei confronti di attacchi di tipo stingray IMSI man-in-the-middle.</string>
    <string name="antiquated_protocols">Il tuo dispositivo è connesso ad una rete che utilizza protocolli antiquati.\n\nE\' esposta a numerose vulnerabilità della sicurezza.</string>
    <string name="voice_network">Rete voce: \u0020 %1$s</string>
    <string name="data_network">Rete dati: \u0020 %1$s</string>
    <string name="additional_network_info">Informazioni aggiuntive sulla rete: \u0020 %1$s</string>

    <!-- Navigation menu. -->
    <string name="settings">Impostazioni</string>
    <string name="protocols">Protocolli</string>
    <string name="logcat">Logcat</string>
    <string name="permissions">Autorizzazioni</string>
    <string name="privacy_policy">Privacy Policy</string>
    <string name="changelog">Changelog</string>
    <string name="licenses">Licenze</string>
    <string name="contributors">Collaboratori</string>
    <string name="news">News</string>
    <string name="roadmap">Roadmap</string>
    <string name="bug_tracker">Bug Tracker</string>
    <string name="forum">Forum</string>
    <string name="donations">Donazioni</string>

    <!-- Protocol families. -->
    <string name="protocol_2g">2G</string>
    <string name="protocol_3g">3G</string>
    <string name="protocol_4g">4G</string>
    <string name="protocol_5g">5G</string>
    <string name="protocol_other">Altri</string>

    <!-- Network types. -->
    <string name="unknown">Sconosciuta</string>
    <string name="gprs">GPRS – 2.5G/3G</string>
    <string name="gprs_detail">General Packet Radio Service</string>
    <string name="edge">EDGE – 2.5G/3G</string>
    <string name="edge_detail">Enhanced Data rates for GSM Evolution</string>
    <string name="umts">UMTS – 3G</string>
    <string name="umts_detail">Universal Mobile Telecommunications System</string>
    <string name="cdma">CDMA – 2G</string>
    <string name="cdma_detail">Code-Division Multiple Access</string>
    <string name="evdo_0">EVDO 0 – 3.5G</string>
    <string name="evdo_0_detail">Evolution-Data Optimized release 0</string>
    <string name="evdo_a">EVDO A – 3.5G</string>
    <string name="evdo_a_detail">Evolution-Data Optimized revision A</string>
    <string name="rtt">1xRTT – 3G</string>
    <string name="rtt_detail">Single-Carrier Radio Transmission Technology</string>
    <string name="hsdpa">HSDPA – 3.5G</string>
    <string name="hsdpa_detail">High Speed Downlink Packet Access</string>
    <string name="hsupa">HSUPA – 3.5G</string>
    <string name="hsupa_detail">High-Speed Uplink Packet Access</string>
    <string name="hspa">HSPA – 3.5G</string>
    <string name="hspa_detail">High Speed Packet Access</string>
    <string name="iden">IDEN – 2G</string>
    <string name="iden_detail">Integrated Digital Enhanced Network</string>
    <string name="evdo_b">EVDO B – 3.5G</string>
    <string name="evdo_b_detail">Evolution-Data Optimized revision B</string>
    <string name="lte">LTE – 4G</string>
    <string name="lte_detail">Long-Term Evolution</string>
    <string name="ehrpd">EHRPD – 3.5G/4G</string>
    <string name="ehrpd_detail">Enhanced High-Rate Packet Data</string>
    <string name="hspap">HSPAP – 3.5G</string>
    <string name="hspap_detail">High Speed Packet Access Plus</string>
    <string name="gsm">GSM – 2G</string>
    <string name="gsm_detail">Global System for Mobile Communications</string>
    <string name="td_scdma">TD-SCDMA – 3G</string>
    <string name="td_scdma_detail">Time Division-Synchronous Code Division Multiple Access</string>
    <string name="iwlan">IWLAN – Wi-Fi</string>
    <string name="iwlan_detail">Interworking Wireless LAN</string>
    <string name="nr">NR – 5G</string>
    <string name="nr_detail">New Radio</string>
    <string name="error">Error</string>

    <!-- Override network types. -->
    <string name="none">Nessuna</string>
    <string name="lte_ca">LTE CA – 4G</string>
    <string name="lte_ca_detail">Long-Term Evolution Carrier Aggregation</string>
    <string name="lte_advanced_pro">LTE Advanced Pro – 4.5G</string>
    <string name="lte_advanced_pro_detail">Long-Term Evolution Advanced Pro</string>
    <string name="nr_nsa">NR NSA – 4G/5G</string>
    <string name="nr_nsa_detail">New Radio Non-Standalone</string>
    <string name="nr_nsa_mmwave">NR NSA mmWave – 4G/5G</string>
    <string name="nr_nsa_mmwave_detail">New Radio Non-Standalone millimeter Wave</string>
    <string name="nr_advanced">NR Advanced – 5G</string>
    <string name="nr_advanced_detail">New Radio Advanced</string>

    <!-- Permission dialogs. -->
    <string name="phone_permission">Autorizzazioni sul dispositivo</string>
    <string name="phone_permission_text">Privacy Cell ha bisogno dell\'autorizzazione di lettura dello stato del telefono per poter determinare il livello si sicurezza della connessione.</string>
    <string name="notification_permission">Autorizzazione per le notifiche</string>
    <string name="notification_permission_text">Privacy Cell ha bisogno dell\'autorizzazione "Post Notification" per mostrare le notifiche del monitoraggio in tempo reale.</string>
    <string name="ok">OK</string>

    <!-- Dialogs. -->
    <string name="stingrays">Stingray</string>
    <string name="antiquated_network_title">Rete Antiquata</string>
    <string name="close">Chiudi</string>

    <!-- Settings. -->
    <string name="monitoring">Monitoraggio</string>
    <string name="realtime_monitoring">Monitoraggio in tempo reale</string>
    <string name="realtime_monitoring_summary">Aggiunge un\'icona nella barra di stato che monitora la rete cellulare.</string>
        <string name="notification_permission_denied">L\'autorizzazione "Post Notification" non è stata al momento concessa per cui le notifiche non saranno visualizzate.</string>
    <string name="secure_network_notification">Notifica di rete sicura</string>
    <string name="insecure_network_notification">Notifica di rete insicura</string>
    <string name="antiquated_network_notification">Notifiche di Rete Antiquata</string>
    <string name="consider_3g_antiquated">Considera il 3G come antiquato</string>
    <string name="consider_3g_antiquated_summary">Considera il protocollo 3G come antiquato. Ad un certo punto in futuro questa impostazione sarà quella di default.
        La modifica di questa impostazione provocherà il riavvio di Privacy Cell.</string>
    <string name="interface_title">Interfaccia</string>
    <string name="bottom_app_bar">Barra dell\'app in basso</string>
    <string name="bottom_app_bar_summary">Sposta la barra dell\'app nella parte inferiore dello schermo.  La modifica di questa impostazione provoca il riavvio di Privacy Cell.</string>

    <!-- Logcat.  Android removes double spaces, but extra spaces can be manually specified with the Unicode `\u0020` formatting.
            The `%1$s` code inserts variables into the displayed text and should be preserved in translation.-->
    <string name="copy_string">Copia</string>
    <string name="save">Salva</string>
    <string name="clear">Cancella</string>
    <string name="logcat_copied">Logcat copiato.</string>
    <string name="privacy_cell_logcat_txt">Privacy Cell Logcat.txt</string>
    <string name="logcat_saved">%1$s salvato.</string>
    <string name="error_saving_logcat">Errore salvataggio logcat: \u0020 %1$s</string>

    <!-- Notifications. -->
    <string name="secure">Sicura</string>
    <string name="insecure">Insicura</string>
    <string name="antiquated">Antiquata</string>
    <string name="secure_network">Sei connesso ad una rete sicura.</string>
    <string name="insecure_network">Sei connesso ad una rete non sicura.</string>
    <string name="antiquated_network">Sei connesso ad una rete antiquata.</string>
    <string name="unknown_network">Il livello di sicurezza della rete è sconosciuto.</string>
    <string name="secure_network_channel">Rete sicura</string>
    <string name="insecure_network_channel">Rete insicura</string>
    <string name="antiquated_network_channel">Rete Antiquata</string>
    <string name="unknown_network_channel">Rete sconosciuta</string>
</resources>
