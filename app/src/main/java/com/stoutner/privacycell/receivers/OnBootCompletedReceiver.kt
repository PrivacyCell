/*
 * Copyright 2021,2023 Soren Stoutner <soren@stoutner.com>.
 *
 * This file is part of Privacy Cell <https://www.stoutner.com/privacy-cell>.
 *
 * Privacy Cell is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Privacy Cell is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Privacy Cell.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.stoutner.privacycell.receivers

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import androidx.preference.PreferenceManager
import com.stoutner.privacycell.R
import com.stoutner.privacycell.services.RealtimeMonitoringService

class OnBootCompletedReceiver : BroadcastReceiver() {
    override fun onReceive(context: Context, intent: Intent) {
        // Check to make sure the system ACTION_BOOT_COMPLETED initiated the receiver.
        if (intent.action == Intent.ACTION_BOOT_COMPLETED) {
            // Get a handle for the shared preferences.
            val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(context)

            // Get the realtime monitoring preference.
            val realtimeMonitoring = sharedPreferences.getBoolean(context.getString(R.string.realtime_monitoring_key), false)

            // Start the realtime monitoring service if it is enabled.
            if (realtimeMonitoring) {
                context.startForegroundService(Intent(context, RealtimeMonitoringService::class.java))
            }
        }
    }
}
