/*
 * Copyright 2021-2022 Soren Stoutner <soren@stoutner.com>.
 *
 * This file is part of Privacy Cell <https://www.stoutner.com/privacy-cell>.
 *
 * Privacy Cell is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Privacy Cell is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Privacy Cell.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.stoutner.privacycell.dialogs

import android.app.Dialog
import android.content.Context
import android.content.DialogInterface
import android.os.Bundle

import androidx.appcompat.app.AlertDialog
import androidx.fragment.app.DialogFragment

import com.stoutner.privacycell.R

class PhonePermissionDialog : DialogFragment() {
    // Declare the listener.
    private lateinit var storagePermissionDialogListener: StoragePermissionDialogListener

    // The public interface is used to send information back to the parent activity.
    interface StoragePermissionDialogListener {
        fun onCloseStoragePermissionDialog()
    }

    override fun onAttach(context: Context) {
        // Run the default commands.
        super.onAttach(context)

        // Get a handle for the listener from the launching context.
        storagePermissionDialogListener = context as StoragePermissionDialogListener
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        // Use a builder to create the alert dialog.
        val dialogBuilder = AlertDialog.Builder(requireContext(), R.style.Theme_PrivacyCellAlertDialog)

        // Set the icon.
        dialogBuilder.setIcon(R.drawable.phone_permission)

        // Set the title.
        dialogBuilder.setTitle(R.string.phone_permission)

        // Set the text.
        dialogBuilder.setMessage(R.string.phone_permission_text)

        // Set the close button listener.
        dialogBuilder.setNegativeButton(R.string.ok) { _: DialogInterface, _: Int ->
            // Call the storage permission dialog listener.
            storagePermissionDialogListener.onCloseStoragePermissionDialog()
        }

        // Return the alert dialog.
        return dialogBuilder.create()
    }
}
