/*
 * Copyright 2022-2023 Soren Stoutner <soren@stoutner.com>.
 *
 * This file is part of Privacy Cell <https://www.stoutner.com/privacy-cell>.
 *
 * Privacy Cell is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Privacy Cell is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Privacy Cell.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.stoutner.privacycell.helpers

import android.content.Context
import android.telephony.TelephonyDisplayInfo
import android.telephony.TelephonyManager

import com.stoutner.privacycell.R

class ProtocolHelper {
    companion object {
        // Define the public static companion object constants.
        const val UNPOPULATED = -1
        const val SECURE = 0
        const val INSECURE = 1
        const val ANTIQUATED = 2
    }

    fun checkNetwork(networkType: Int, consider3gAntiquated: Boolean): Int {
        if ((networkType == TelephonyManager.NETWORK_TYPE_NR) ||
            (networkType == TelephonyManager.NETWORK_TYPE_IWLAN) ||
            (networkType == TelephonyManager.NETWORK_TYPE_UNKNOWN)) {
            // The network is secure.
            return SECURE
        } else if (networkType == TelephonyManager.NETWORK_TYPE_LTE) {
            // The network is insecure.
            return INSECURE
        } else if (!consider3gAntiquated &&
                  ((networkType == TelephonyManager.NETWORK_TYPE_1xRTT) ||
                   (networkType == TelephonyManager.NETWORK_TYPE_EVDO_0) ||
                   (networkType == TelephonyManager.NETWORK_TYPE_EVDO_A) ||
                   (networkType == TelephonyManager.NETWORK_TYPE_EVDO_B) ||
                   (networkType == TelephonyManager.NETWORK_TYPE_EHRPD) ||
                   (networkType == TelephonyManager.NETWORK_TYPE_UMTS) ||
                   (networkType == TelephonyManager.NETWORK_TYPE_TD_SCDMA) ||
                   (networkType == TelephonyManager.NETWORK_TYPE_HSDPA) ||
                   (networkType == TelephonyManager.NETWORK_TYPE_HSUPA) ||
                   (networkType == TelephonyManager.NETWORK_TYPE_HSPA) ||
                   (networkType == TelephonyManager.NETWORK_TYPE_HSPAP))) {
            // The network is insecure.
            return INSECURE
        } else {
            // Either 3G networks are considered antiquated, or this is a 2G network.
            // TelephonyManager.NETWORK_TYPE_GPRS
            // TelephonyManager.NETWORK_TYPE_EDGE
            // TelephonyManager.NETWORK_TYPE_CDMA
            // TelephonyManager.NETWORK_TYPE_IDEN
            // TelephonyManager.NETWORK_TYPE_GSM
            return ANTIQUATED
        }
    }

    fun checkAdditionalNetworkInfo(additionalNetworkInfoType: Int): Int {
        return if ((additionalNetworkInfoType == TelephonyDisplayInfo.OVERRIDE_NETWORK_TYPE_NONE) ||
                   (additionalNetworkInfoType == TelephonyDisplayInfo.OVERRIDE_NETWORK_TYPE_NR_ADVANCED)) {
            // The additional network info is secure.
            SECURE
        } else {
            // The additional network info is insecure.
            // TelephonyDisplayInfo.OVERRIDE_NETWORK_TYPE_LTE_CA
            // TelephonyDisplayInfo.OVERRIDE_NETWORK_TYPE_LTE_ADVANCED_PRO
            // TelephonyDisplayInfo.OVERRIDE_NETWORK_TYPE_NR_NSA
            // TelephonyDisplayInfo.OVERRIDE_NETWORK_TYPE_NR_NSA_MMWAVE -- Can be removed once the minimum API >= 31.
            INSECURE
        }
    }

    fun getNetworkTypeStringArray(networkType: Int, context: Context) : Array<String> {
        // Return the string array that corresponds to the network type.  The deprecated `NETWORK_TYPE_IDEN` can be removed once the minimum API >= 34.
        @Suppress("DEPRECATION")
        return when(networkType) {
            TelephonyManager.NETWORK_TYPE_UNKNOWN -> arrayOf(context.getString(R.string.unknown), "")
            TelephonyManager.NETWORK_TYPE_GPRS -> arrayOf(context.getString(R.string.gprs), context.getString(R.string.gprs_detail))
            TelephonyManager.NETWORK_TYPE_EDGE -> arrayOf(context.getString(R.string.edge), context.getString(R.string.edge_detail))
            TelephonyManager.NETWORK_TYPE_UMTS -> arrayOf(context.getString(R.string.umts), context.getString(R.string.umts_detail))
            TelephonyManager.NETWORK_TYPE_CDMA -> arrayOf(context.getString(R.string.cdma), context.getString(R.string.cdma_detail))
            TelephonyManager.NETWORK_TYPE_EVDO_0 -> arrayOf(context.getString(R.string.evdo_0), context.getString(R.string.evdo_0_detail))
            TelephonyManager.NETWORK_TYPE_EVDO_A -> arrayOf(context.getString(R.string.evdo_a), context.getString(R.string.evdo_a_detail))
            TelephonyManager.NETWORK_TYPE_1xRTT -> arrayOf(context.getString(R.string.rtt), context.getString(R.string.rtt_detail))
            TelephonyManager.NETWORK_TYPE_HSDPA -> arrayOf(context.getString(R.string.hsdpa), context.getString(R.string.hsdpa_detail))
            TelephonyManager.NETWORK_TYPE_HSUPA -> arrayOf(context.getString(R.string.hsupa), context.getString(R.string.hsupa_detail))
            TelephonyManager.NETWORK_TYPE_HSPA -> arrayOf(context.getString(R.string.hspa), context.getString(R.string.hspa_detail))
            TelephonyManager.NETWORK_TYPE_IDEN -> arrayOf(context.getString(R.string.iden), context.getString(R.string.iden_detail))
            TelephonyManager.NETWORK_TYPE_EVDO_B -> arrayOf(context.getString(R.string.evdo_b), context.getString(R.string.evdo_b_detail))
            TelephonyManager.NETWORK_TYPE_LTE -> arrayOf(context.getString(R.string.lte), context.getString(R.string.lte_detail))
            TelephonyManager.NETWORK_TYPE_EHRPD -> arrayOf(context.getString(R.string.ehrpd), context.getString(R.string.ehrpd_detail))
            TelephonyManager.NETWORK_TYPE_HSPAP -> arrayOf(context.getString(R.string.hspap), context.getString(R.string.hspap_detail))
            TelephonyManager.NETWORK_TYPE_GSM -> arrayOf(context.getString(R.string.gsm), context.getString(R.string.gsm_detail))
            TelephonyManager.NETWORK_TYPE_TD_SCDMA -> arrayOf(context.getString(R.string.td_scdma), context.getString(R.string.td_scdma_detail))
            TelephonyManager.NETWORK_TYPE_IWLAN -> arrayOf(context.getString(R.string.iwlan), context.getString(R.string.iwlan_detail))
            TelephonyManager.NETWORK_TYPE_NR -> arrayOf(context.getString(R.string.nr), context.getString(R.string.nr_detail))
            else -> arrayOf(context.getString(R.string.error), "")
        }
    }

    fun getAdditionalNetworkInfoStringArray(overrideNetworkType: Int, context: Context) : Array<String> {
        // Return the string array that corresponds to the override network type.
        @Suppress("DEPRECATION")  // OVERRIDE_NETWORK_TYPE_NR_NSA_MMWAVE can be removed once the minimum API >= 31.
        return when(overrideNetworkType) {
            TelephonyDisplayInfo.OVERRIDE_NETWORK_TYPE_NONE -> arrayOf(context.getString(R.string.none), "")
            TelephonyDisplayInfo.OVERRIDE_NETWORK_TYPE_LTE_CA -> arrayOf(context.getString(R.string.lte_ca), context.getString(R.string.lte_ca_detail))
            TelephonyDisplayInfo.OVERRIDE_NETWORK_TYPE_LTE_ADVANCED_PRO -> arrayOf(context.getString(R.string.lte_advanced_pro), context.getString(R.string.lte_advanced_pro_detail))
            TelephonyDisplayInfo.OVERRIDE_NETWORK_TYPE_NR_NSA -> arrayOf(context.getString(R.string.nr_nsa), context.getString(R.string.nr_nsa_detail))
            // OVERRIDE_NETWORK_TYPE_NR_NSA_MMWAVE can be removed once the minimum API >= 31.
            TelephonyDisplayInfo.OVERRIDE_NETWORK_TYPE_NR_NSA_MMWAVE -> arrayOf(context.getString(R.string.nr_nsa_mmwave), context.getString(R.string.nr_nsa_mmwave_detail))
            TelephonyDisplayInfo.OVERRIDE_NETWORK_TYPE_NR_ADVANCED -> arrayOf(context.getString(R.string.nr_advanced), context.getString(R.string.nr_advanced_detail))
            else -> arrayOf(context.getString(R.string.error), "")
        }
    }
}
