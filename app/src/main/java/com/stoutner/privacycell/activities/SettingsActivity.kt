/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2021-2022 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Cell <https://www.stoutner.com/privacy-cell>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.stoutner.privacycell.activities

import android.Manifest
import android.content.Intent
import android.content.pm.PackageManager
import android.os.Build
import android.os.Bundle

import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.preference.PreferenceManager

import com.stoutner.privacycell.R
import com.stoutner.privacycell.dialogs.NotificationPermissionDialog.NotificationPermissionDialogListener
import com.stoutner.privacycell.fragments.SettingsFragment
import com.stoutner.privacycell.services.RealtimeMonitoringService

class SettingsActivity : AppCompatActivity(), NotificationPermissionDialogListener {
    // Declare the class variables.
    private lateinit var settingsFragment: SettingsFragment

    override fun onCreate(savedInstanceState: Bundle?) {
        // Run the default commands.
        super.onCreate(savedInstanceState)

        // Get a handle for the shared preferences.
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)

        // Get the bottom app bar preference.
        val bottomAppBar = sharedPreferences.getBoolean(getString(R.string.bottom_app_bar_key), false)

        // Set the content view.
        if (bottomAppBar) {
            setContentView(R.layout.settings_bottom_appbar)
        } else {
            setContentView(R.layout.settings_top_appbar)
        }

        // Get a handle for the toolbar.
        val toolbar = findViewById<Toolbar>(R.id.toolbar)

        // Set the support action bar.
        setSupportActionBar(toolbar)

        // Get a handle for the action bar.
        val actionBar = supportActionBar!!

        // Display the home arrow on the action bar.
        actionBar.setDisplayHomeAsUpEnabled(true)

        // Instantiate the settings fragment.
        settingsFragment = SettingsFragment()

        // Load the settings fragment.
        supportFragmentManager.beginTransaction().replace(R.id.preferences_framelayout, settingsFragment).commitNow()
    }

    override fun onCloseNotificationPermissionDialog() {
        // Request the post notifications permission if the API >= 33.
        if (Build.VERSION.SDK_INT >= 33) {
            ActivityCompat.requestPermissions(this, arrayOf(Manifest.permission.POST_NOTIFICATIONS), PrivacyCellActivity.NOTIFICATION_PERMISSION_REQUEST_CODE)
        }
    }

    override fun onRequestPermissionsResult(requestCode: Int, permissions: Array<out String>, grantResults: IntArray) {
        // Run the default commands.
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        // Only process the results if they exist (this method is triggered when a dialog is presented the first time for an app, but no grant results are included)
        // and the result is for the notification permission.
        if (grantResults.isNotEmpty() && (requestCode == PrivacyCellActivity.NOTIFICATION_PERMISSION_REQUEST_CODE)) {
            // Check to see if the notification permission was granted.  If the dialog was canceled the grant result will be empty.
            if (grantResults[0] == PackageManager.PERMISSION_GRANTED) {  // The notification permission was granted.
                // Start the realtime monitoring service.
                startService(Intent(this, RealtimeMonitoringService::class.java))

                // Update the realtime monitoring preference summary.
                settingsFragment.updateRealtimeMonitoringSummary()
            }
        }
    }
}
