/* SPDX-License-Identifier: GPL-3.0-or-later
 * SPDX-FileCopyrightText: 2022 Soren Stoutner <soren@stoutner.com>
 *
 * This file is part of Privacy Cell <https://www.stoutner.com/privacy-cell>.
 *
 * This program is free software: you can redistribute it and/or modify it under
 * the terms of the GNU General Public License as published by the Free Software
 * Foundation, either version 3 of the License, or (at your option) any later
 * version.
 *
 * This program is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE.  See the GNU General Public License for more
 * details.
 *
 * You should have received a copy of the GNU General Public License along with
 * this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package com.stoutner.privacycell.activities

import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ListView
import android.widget.TextView

import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.preference.PreferenceManager

import com.stoutner.privacycell.R
import com.stoutner.privacycell.adapters.ProtocolArrayAdapter
import com.stoutner.privacycell.dataclasses.Protocol
import com.stoutner.privacycell.dialogs.WebViewDialog

class ProtocolsActivity : AppCompatActivity() {
    public override fun onCreate(savedInstanceState: Bundle?) {
        // Run the default commands.
        super.onCreate(savedInstanceState)

        // Get a handle for the shared preferences.
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)

        // Get the bottom app bar preference.
        val bottomAppBar = sharedPreferences.getBoolean(getString(R.string.bottom_app_bar_key), false)

        // Set the content view.
        if (bottomAppBar) {
            setContentView(R.layout.protocols_bottom_appbar)
        } else {
            setContentView(R.layout.protocols_top_appbar)
        }

        // Get handles for the views.
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        val listView = findViewById<ListView>(R.id.listview)

        // Set the toolbar as the action bar.
        setSupportActionBar(toolbar)

        // Get a handle for the action bar.
        val actionBar = supportActionBar!!

        // Display the back arrow in the action bar.
        actionBar.setDisplayHomeAsUpEnabled(true)

        // Create a protocol array list.
        val protocolArrayList = ArrayList<Protocol>()

        // Populate the protocol array list.  Headers are followed by `true`.
        protocolArrayList.add(Protocol(getString(R.string.protocol_2g), isHeader = true, isAdditionalNetworkInfo = false))
        protocolArrayList.add(Protocol(getString(R.string.cdma_detail), isHeader = false, isAdditionalNetworkInfo = false))
        protocolArrayList.add(Protocol(getString(R.string.gsm_detail), isHeader = false, isAdditionalNetworkInfo = false))
        protocolArrayList.add(Protocol(getString(R.string.iden_detail), isHeader = false, isAdditionalNetworkInfo = false))
        protocolArrayList.add(Protocol(getString(R.string.gprs_detail), isHeader = false, isAdditionalNetworkInfo = false))
        protocolArrayList.add(Protocol(getString(R.string.edge_detail), isHeader = false, isAdditionalNetworkInfo = false))

        protocolArrayList.add(Protocol(getString(R.string.protocol_3g), isHeader = true, isAdditionalNetworkInfo = false))
        protocolArrayList.add(Protocol(getString(R.string.rtt_detail), isHeader = false, isAdditionalNetworkInfo = false))
        protocolArrayList.add(Protocol(getString(R.string.evdo_0_detail), isHeader = false, isAdditionalNetworkInfo = false))
        protocolArrayList.add(Protocol(getString(R.string.evdo_a_detail), isHeader = false, isAdditionalNetworkInfo = false))
        protocolArrayList.add(Protocol(getString(R.string.evdo_b_detail), isHeader = false, isAdditionalNetworkInfo = false))
        protocolArrayList.add(Protocol(getString(R.string.ehrpd_detail), isHeader = false, isAdditionalNetworkInfo = false))
        protocolArrayList.add(Protocol(getString(R.string.umts_detail), isHeader = false, isAdditionalNetworkInfo = false))
        protocolArrayList.add(Protocol(getString(R.string.td_scdma_detail), isHeader = false, isAdditionalNetworkInfo = false))
        protocolArrayList.add(Protocol(getString(R.string.hsdpa_detail), isHeader = false, isAdditionalNetworkInfo = false))
        protocolArrayList.add(Protocol(getString(R.string.hsupa_detail), isHeader = false, isAdditionalNetworkInfo = false))
        protocolArrayList.add(Protocol(getString(R.string.hspa_detail), isHeader = false, isAdditionalNetworkInfo = false))
        protocolArrayList.add(Protocol(getString(R.string.hspap_detail), isHeader = false, isAdditionalNetworkInfo = false))

        protocolArrayList.add(Protocol(getString(R.string.protocol_4g), isHeader = true, isAdditionalNetworkInfo = false))
        protocolArrayList.add(Protocol(getString(R.string.lte_detail), isHeader = false, isAdditionalNetworkInfo = false))
        protocolArrayList.add(Protocol(getString(R.string.lte_ca_detail), isHeader = false, isAdditionalNetworkInfo = true))
        protocolArrayList.add(Protocol(getString(R.string.lte_advanced_pro_detail), isHeader = false, isAdditionalNetworkInfo = true))
        protocolArrayList.add(Protocol(getString(R.string.nr_nsa_detail), isHeader = false, isAdditionalNetworkInfo = true))
        protocolArrayList.add(Protocol(getString(R.string.nr_nsa_mmwave_detail), isHeader = false, isAdditionalNetworkInfo = true))

        protocolArrayList.add(Protocol(getString(R.string.protocol_5g), isHeader = true, isAdditionalNetworkInfo = false))
        protocolArrayList.add(Protocol(getString(R.string.nr_detail), isHeader = false, isAdditionalNetworkInfo = false))
        protocolArrayList.add(Protocol(getString(R.string.nr_advanced_detail), isHeader = false, isAdditionalNetworkInfo = true))

        protocolArrayList.add(Protocol(getString(R.string.protocol_other), isHeader = true, isAdditionalNetworkInfo = false))
        protocolArrayList.add(Protocol(getString(R.string.iwlan_detail), isHeader = false, isAdditionalNetworkInfo = false))

        // Instantiate the protocol array adapter.
        val protocolArrayAdapter = ProtocolArrayAdapter(this, protocolArrayList)

        // Set the list view adapter.
        listView.adapter = protocolArrayAdapter

        // Listen for clicks on items in the list view.
        listView.onItemClickListener = AdapterView.OnItemClickListener { _: AdapterView<*>?, view: View, _: Int, _: Long ->
            // Get a handle for the text view.
            val textview = view.findViewById<TextView>(R.id.textview)

            // Show an alert dialog if a protocol item was selected.
            when (textview.text) {
                getString(R.string.gprs_detail) -> WebViewDialog().type(WebViewDialog.NETWORK_GPRS)
                getString(R.string.edge_detail) -> WebViewDialog().type(WebViewDialog.NETWORK_EDGE)
                getString(R.string.umts_detail) -> WebViewDialog().type(WebViewDialog.NETWORK_UMTS)
                getString(R.string.cdma_detail) -> WebViewDialog().type(WebViewDialog.NETWORK_CDMA)
                getString(R.string.evdo_0_detail) -> WebViewDialog().type(WebViewDialog.NETWORK_EVDO_0)
                getString(R.string.evdo_a_detail) -> WebViewDialog().type(WebViewDialog.NETWORK_EVDO_A)
                getString(R.string.rtt_detail) -> WebViewDialog().type(WebViewDialog.NETWORK_1xRTT)
                getString(R.string.hsdpa_detail) -> WebViewDialog().type(WebViewDialog.NETWORK_HSDPA)
                getString(R.string.hsupa_detail) -> WebViewDialog().type(WebViewDialog.NETWORK_HSUPA)
                getString(R.string.hspa_detail) -> WebViewDialog().type(WebViewDialog.NETWORK_HSPA)
                getString(R.string.iden_detail) -> WebViewDialog().type(WebViewDialog.NETWORK_IDEN)
                getString(R.string.evdo_b_detail) -> WebViewDialog().type(WebViewDialog.NETWORK_EVDO_B)
                getString(R.string.lte_detail) -> WebViewDialog().type(WebViewDialog.NETWORK_LTE)
                getString(R.string.ehrpd_detail) -> WebViewDialog().type(WebViewDialog.NETWORK_EHRPD)
                getString(R.string.hspap_detail) -> WebViewDialog().type(WebViewDialog.NETWORK_HSPAP)
                getString(R.string.gsm_detail) -> WebViewDialog().type(WebViewDialog.NETWORK_GSM)
                getString(R.string.td_scdma_detail) -> WebViewDialog().type(WebViewDialog.NETWORK_TD_SCDMA)
                getString(R.string.iwlan_detail) -> WebViewDialog().type(WebViewDialog.NETWORK_IWLAN)
                getString(R.string.nr_detail) -> WebViewDialog().type(WebViewDialog.NETWORK_NR)
                getString(R.string.lte_ca_detail) -> WebViewDialog().type(WebViewDialog.OVERRIDE_NETWORK_LTE_CA)
                getString(R.string.lte_advanced_pro_detail) -> WebViewDialog().type(WebViewDialog.OVERRIDE_NETWORK_LTE_ADVANCED_PRO)
                getString(R.string.nr_nsa_detail) -> WebViewDialog().type(WebViewDialog.OVERRIDE_NETWORK_NR_NSA)
                getString(R.string.nr_nsa_mmwave_detail) -> WebViewDialog().type(WebViewDialog.OVERRIDE_NETWORK_NR_NSA_MMWAVE)
                getString(R.string.nr_advanced_detail) -> WebViewDialog().type(WebViewDialog.OVERRIDE_NETWORK_NR_ADVANCED)
                else -> null
            }?.show(supportFragmentManager, getString(R.string.protocols))
        }
    }
}
