/*
 * Copyright 2021-2023 Soren Stoutner <soren@stoutner.com>.
 *
 * This file is part of Privacy Cell <https://www.stoutner.com/privacy-cell>.
 *
 * Privacy Cell is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Privacy Cell is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Privacy Cell.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.stoutner.privacycell.workers

import android.content.Context
import android.content.Intent

import androidx.preference.PreferenceManager
import androidx.work.WorkManager
import androidx.work.Worker
import androidx.work.WorkerParameters

import com.stoutner.privacycell.R
import com.stoutner.privacycell.services.RealtimeMonitoringService

class RestartServiceWorker(appContext: Context, workerParameters: WorkerParameters) : Worker(appContext, workerParameters) {
    override fun doWork(): Result {
        // Get a handle for the shared preferences.
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(applicationContext)

        // Check to see if realtime monitoring is enabled.  Sometimes the shared preferences can't return a value in time, because Android sucks.
        // So, the default value is set to true, which is the safest value if the shared preferences can't be queried.
        if (sharedPreferences.getBoolean(applicationContext.getString(R.string.realtime_monitoring_key), true)) {  // Realtime monitoring is enabled.
            // Start the realtime monitoring service as a foreground service, which is required because the worker is running in the background.
            applicationContext.startForegroundService(Intent(applicationContext, RealtimeMonitoringService::class.java))
        } else {  // Realtime monitoring is disabled.
            // Stop the realtime monitoring service.
            applicationContext.stopService(Intent(applicationContext, RealtimeMonitoringService::class.java))

            // Cancel the realtime listener work request.
            WorkManager.getInstance(applicationContext).cancelUniqueWork(applicationContext.getString(R.string.register_listener_work_request))
        }

        // Return a success.
        return Result.success()
    }
}
