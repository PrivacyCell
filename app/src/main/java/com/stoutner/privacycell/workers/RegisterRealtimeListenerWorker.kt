/*
 * Copyright 2021-2023 Soren Stoutner <soren@stoutner.com>.
 *
 * This file is part of Privacy Cell <https://www.stoutner.com/privacy-cell>.
 *
 * Privacy Cell is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Privacy Cell is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Privacy Cell.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.stoutner.privacycell.workers

import android.app.ActivityManager
import android.content.ComponentName
import android.content.Context
import android.content.Intent
import android.content.ServiceConnection
import android.os.IBinder

import androidx.preference.PreferenceManager
import androidx.work.OneTimeWorkRequestBuilder
import androidx.work.OutOfQuotaPolicy
import androidx.work.WorkManager
import androidx.work.Worker
import androidx.work.WorkerParameters

import com.stoutner.privacycell.R
import com.stoutner.privacycell.services.RealtimeMonitoringService

class RegisterRealtimeListenerWorker(appContext: Context, workerParameters: WorkerParameters) : Worker(appContext, workerParameters) {
    override fun doWork(): Result {
        // Get a handle for the shared preferences.
        val sharedPreferences = PreferenceManager.getDefaultSharedPreferences(applicationContext)

        // Get the realtime monitoring preference.  Sometimes the shared preferences can't return a value in time, because Android sucks.
        // So, the default value is set to true, which is the safest value if the shared preferences can't be queried.
        val realtimeMonitoring = sharedPreferences.getBoolean(applicationContext.getString(R.string.realtime_monitoring_key), true)

        // Perform the functions according to the realtime monitoring status.
        @Suppress("DEPRECATION")  // The deprecated `getRunningServices()` now only returns services started by Privacy Cell, but that is all we want to know anyway.
        if (realtimeMonitoring) {  // Realtime monitoring is enabled.
            // Get a handle for the activity manager.
            val activityManager: ActivityManager = applicationContext.getSystemService(Context.ACTIVITY_SERVICE) as ActivityManager

            // Get a list of the running service info.  The deprecated `getRunningServices()` now only returns services started by Privacy Cell, but that is all we want to know anyway.
            val runningServiceInfoList: List<ActivityManager.RunningServiceInfo> = activityManager.getRunningServices(1)

            // Check to see if the service is currently running.
            if (runningServiceInfoList.isEmpty()) {  // The service is currently stopped.
                // Create a restart service work request.  This task can't be run from here because of Android 12's foreground service restrictions on non-expedited workers.
                val restartServiceWorkRequest = OneTimeWorkRequestBuilder<RestartServiceWorker>().apply {
                    // Set the status to be expedited so that it can start a foreground service.
                    setExpedited(OutOfQuotaPolicy.RUN_AS_NON_EXPEDITED_WORK_REQUEST)
                }.build()

                // Make it so.
                WorkManager.getInstance(applicationContext).enqueue(restartServiceWorkRequest)
            } else {  // The service is currently running.
                // Create a service connection.
                val serviceConnection = object : ServiceConnection {
                    override fun onServiceConnected(componentName: ComponentName, serviceIBinder: IBinder) {
                        // Get a handle for the realtime monitoring service binder.
                        val realtimeMonitoringServiceBinder = serviceIBinder as RealtimeMonitoringService.ServiceBinder

                        // Get a handle for the realtime monitoring service.
                        val realtimeMonitoringService = realtimeMonitoringServiceBinder.getService()

                        // Register the telephony manager listener.
                        realtimeMonitoringService.registerTelephonyManagerListener()

                        // Populate the notification, which might have been accidentally dismissed by the user beginning in Android 14.
                        realtimeMonitoringService.populateNotification()

                        // Unbind the service.
                        applicationContext.unbindService(this)
                    }

                    override fun onServiceDisconnected(componentName: ComponentName) {
                        // Do nothing.
                    }
                }

                // Bind to the realtime monitoring service.
                applicationContext.bindService(Intent(applicationContext, RealtimeMonitoringService::class.java), serviceConnection, 0)
            }
        } else {  // Realtime monitoring is disabled.
            // Stop the realtime monitoring service.
            applicationContext.stopService(Intent(applicationContext, RealtimeMonitoringService::class.java))

            // Cancel the realtime listener work request.
            WorkManager.getInstance(applicationContext).cancelUniqueWork(applicationContext.getString(R.string.register_listener_work_request))
        }

        // Return a success.
        return Result.success()
    }
}
