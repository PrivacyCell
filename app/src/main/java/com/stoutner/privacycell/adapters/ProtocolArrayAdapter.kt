/*
 * Copyright © 2021-2022 Soren Stoutner <soren@stoutner.com>.
 *
 * This file is part of Privacy Cell <https://www.stoutner.com/privacy-cell>.
 *
 * Privacy Cell is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Privacy Cell is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with Privacy Cell.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.stoutner.privacycell.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ArrayAdapter
import android.widget.TextView

import com.stoutner.privacycell.R
import com.stoutner.privacycell.dataclasses.Protocol

class ProtocolArrayAdapter(context: Context, protocolArrayList: ArrayList<Protocol>) : ArrayAdapter<Protocol>(context, 0, protocolArrayList) {
    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        // Get a handle for the protocol.
        val protocol = getItem(position)!!

        // Create a populated view and inflate the layout.
        val populatedView = if (protocol.isHeader) {  // The protocol is a header entry.
            LayoutInflater.from(context).inflate(R.layout.protocol_header_textview, parent, false)
        } else {  // The protocol is not a header entry.
            LayoutInflater.from(context).inflate(R.layout.protocol_item_textview, parent, false)
        }

        // Get a handle for the text view.
        val textView = populatedView.findViewById<TextView>(R.id.textview)

        // Populate the text view with the corresponding item from the protocol array list.
        textView.text = protocol.protocolName

        // Change the text view background if the protocol is additional network info.
        if (protocol.isAdditionalNetworkInfo) {
            textView.setBackgroundColor(context.getColor(R.color.additional_network_info))
        }

        // Return the populated view.
        return populatedView
    }
}